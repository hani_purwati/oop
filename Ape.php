<?php
class Ape extends Animal{
	public function __construct($string){
		parent::set_name($string);
		parent::set_legs(2);
	}
	public function yell(){
		return "Auooo";
	}
}
?>